-- migrate:up

create table if not exists username_history (
  id serial not null primary key,
  uid varchar(40) not null,
  name text,
  changed timestamp without time zone,
  required_by_admin boolean,
  foreign key (uid) references "user" (uid)
);

create index if not exists usernamehistory_uid
  on username_history (uid);
create index usernamehistory_name
  on username_history (name);
create index usernamehistory_name_lower
  on username_history (lower((name)::text));

grant select, insert, update, delete
  on table username_history
  to ovarit_auth;

grant select
  on table username_history
  to throat_be;

grant select
  on table username_history
  to throat_py;

insert into site_metadata (key, value)
values ('site.username_change.limit', '0'),
       ('site.username_change.limit_days', '0'),
       ('site.username_change.display_days', '0');

-- migrate:down

drop table if exists username_history cascade;

delete from site_metadata
 where key in ('site.username_change.limit',
               'site.username_change.limit_days',
               'site.username_change.display_days');
