-- migrate:up

create table if not exists required_name_change (
  id serial not null primary key,
  uid text not null,
  admin_uid text not null,
  message text not null,
  required_at timestamp without time zone not null,
  completed_at timestamp without time zone,
  notification_mtid integer,
  foreign key (uid) references public.user (uid),
  foreign key (admin_uid) references public.user (uid),
  foreign key (notification_mtid) references message_thread (mtid)
);

create index if not exists requirednamechange_uid
  on required_name_change (uid);

grant select, insert, update, delete
  on table required_name_change
  to throat_be;

grant select, insert, update, delete
  on table required_name_change
  to ovarit_auth;

alter table username_history
  add column required_id integer;

alter table username_history
  add constraint username_history_required_id_fkey
  foreign key (required_id)
  references required_name_change (id);

alter table username_history
  drop column required_by_admin;

-- migrate:down

alter table username_history
  add column required_by_admin boolean default false;

alter table username_history
  drop column required_id cascade;

drop table required_name_change cascade;
