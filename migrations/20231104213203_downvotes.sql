-- migrate:up

-- Add configuration for automated downvote notifications for admins.
insert into site_metadata (key, value)
values ('site.downvotes.personal.count', '10'),
       ('site.downvotes.personal.days', '7'),
       ('site.downvotes.count', '100'),
       ('site.downvotes.days', '1'),
       ('site.downvotes.silence', '1'),
       ('site.downvotes.thread.percent', '50'),
       ('site.downvotes.thread.comments', '10');

-- migrate:down

delete from site_metadata
 where key in (
   'site.downvotes.personal.count',
   'site.downvotes.personal.days',
   'site.downvotes.count',
   'site.downvotes.days',
   'site.downvotes.silence',
   'site.downvotes.thread.percent',
   'site.downvotes.thread.comments');
