-- migrate:up

create table url_metadata_log (
  id serial not null primary key,
  url text not null,
  fetched timestamp without time zone not null,
  error text
);

create index url_metadata_log_url on url_metadata_log (url);

grant select, insert, update, delete
  on table url_metadata_log
  to throat_be;

grant select, insert, update, delete
  on table url_metadata_log
  to throat_py;

create table url_metadata (
  id serial not null primary key,
  log_id integer not null,
  key text not null,
  value text not null,
  foreign key (log_id) references url_metadata_log (id)
);

create index url_metadata_log_id on url_metadata (log_id);
-- for searches on duplicate canonical urls
create index url_metadata_value on url_metadata (value);

grant select, insert, update, delete
  on table url_metadata
  to throat_be;

grant select, insert, update, delete
  on table url_metadata
  to throat_py;

-- migrate:down

drop table url_metadata cascade;
drop table url_metadata_log cascade;
