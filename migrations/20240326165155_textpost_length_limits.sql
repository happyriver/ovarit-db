-- migrate:up

insert into site_metadata (key, value)
values ('site.text_post.min_length', '0'),
       ('site.text_post.max_length', '65535');

-- migrate:down

delete from site_metadata
 where key in ('site.text_post.min_length',
               'site.text_post.max_length');
