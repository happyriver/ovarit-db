-- migrate:up transaction:false

create index concurrently sub_post_comment_best_score
  on sub_post_comment (best_score(upvotes, downvotes, views));

-- migrate:down

drop index if exists sub_post_comment_best_score;
