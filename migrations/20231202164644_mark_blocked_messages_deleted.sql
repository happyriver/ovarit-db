-- migrate:up

-- The block users feature has been allowing the blocked user to send
-- messages to the blocking user and then not showing those messages
-- in the blocking user's inbox.  This is going to be changed to
-- prevent the sending and omit the check for blocking when showing
-- the inbox.  Mark all the existing blocked messages as read and
-- deleted.
with
  blocked as (
    select message.mid, message.receivedby
      from message
           join user_message_block
               on user_message_block.uid = receivedby
               and user_message_block.target = sentby
     where mtype = 100
  ),
  remove_unreads as (
    delete from user_unread_message
     using blocked
     where user_unread_message.mid = blocked.mid
       and user_unread_message.uid = blocked.receivedby
  )
update user_message_mailbox
   set mailbox = 205
  from blocked
 where user_message_mailbox.mid = blocked.mid
   and user_message_mailbox.uid = blocked.receivedby;

-- migrate:down
