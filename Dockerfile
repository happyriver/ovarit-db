FROM amacneil/dbmate

COPY . /ovarit-db
WORKDIR /ovarit-db

RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["/ovarit-db/entrypoint.sh"]