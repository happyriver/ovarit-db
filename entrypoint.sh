#!/bin/sh

# Function to URL encode the password
urlencode() {
    local length="${#1}"
    local i=0
    while [ $i -lt $length ]; do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c"
        esac
	i=$((i + 1))
    done
}

# URL encode the password
ENCODED_PASSWORD=$(urlencode "${DATABASE_PASSWORD}")

export DATABASE_URL="postgres://${DATABASE_USER}:${ENCODED_PASSWORD}@${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}"
export DBMATE_MIGRATIONS_DIR="/ovarit-db/migrations"
export DBMATE_NO_DUMP_SCHEMA=1

exec dbmate "$@"
